# complexity.py
#
# language: python 3
# author: allen b downet
# implementation: bue
# date: 2015-05-05
# license: >= GPLv3
#
# description: code form allen b downet book think complexity


class Vertex(object):
    """
    a vertex is a node in a graph.
    basically string.
    """
    def __init__(self, s_label=""):
        print("bue: type s_label", type(s_label))
        self.label = s_label
        #self.color = None

    def __repr__(self):
        """
        returns a string representation of this object that can
        be evaluated as a python expression.
        """
        return("vertex(%s)" % repr(self.label))
    __str__ = __repr__
    """
    the str and repr forms of this object are the same.
    """


class Edge(tuple):
    """
    an edge is a tuple of two vertices.
    basically a tuple of strings
    """
    def __new__(cls, *to_v):  # (o_e1, o_e2)
        """
        the edge constructor takes two vertices.
        """
        print("bue: type to_v", type(to_v), type(to_v[0]),  type(to_v[1]))
        if (len(to_v) != 2):
            raise(ValueError, "Edges must connect exactly two vertices.")
        return(tuple.__new__(cls, to_v))  # (o_e1, o_e2)

    def __repr__(self):
        """
        return a string representation of this object that can
        be evaluated as a python expression.
        """
        return("edge(%s,%s)" % (repr(self[0]), repr(self[1])))

    __str__ = __repr__
    """
    the str and repr forms of this object are the same.
    """


class Graph(dict):
    """
    a graph is a dictionary of dictionaries.  the outer
    dictionary maps from a vertex to an inner dictionary.
    the inner dictionary maps from other vertices to edges.
    for vertices a and b, graph[a][b] maps
    to the edge that connects a->b, if it exists.
    """
    def add_vertex(self, o_v):
        """
        add a vertex (o_v) to the graph
        input: 1 vertex string
        """
        print("bue: type o_v", type(o_v))
        self[o_v] = {}

    def add_edge(self, o_e):
        """
        add an edge (o_e) to the graph by adding an entry in both directions.
        if there is already an edge connecting these vertices, the
        new edge replaces it.
        input: 1 edge (which is a vertex tuple)
        """
        print("bue: type o_e", type(o_e), type(o_e[0]), type(o_e[1]))
        o_v, o_w = o_e
        self[o_v][o_w] = o_e
        self[o_w][o_v] = o_e

    def get_edge(self, o_v, o_w):
        """
        (o_v and o_w) are vertices
        input: 2 vertex strings
        """
        print("bue: type o_v o_w", type(o_v), type(o_w))
        try:
            o_e = self[o_v][o_w]
        except:
            o_e = None
        return(o_e)

    def remove_edge(self, o_e):
        """
        (o_e) is an edge
        input: 1 edge (which is a vertex tuple)
        """
        print("bue: type o_e", type(o_e))
        for o_v in o_e:
            print("bue o_v", o_v)
            del(self[o_v])

    def vertices(self):
        """
        list vertices
        """
        lo_v = list(self.keys())
        return(lo_v)

    def edges(self):
        """
        list edges
        """
        lo_e = []
        for v in self:
            for w in self[v]:
                o_e = self.get_edge(v, w)
                lo_e.append(o_e)
        return(lo_e)

    def out_vertices(self, o_v):
        """
        list outer vertices
        """
        lo_x = []
        lo_w = self.vertices()
        for o_w in lo_w:
            o_e = self.get_edge(o_v, o_w)
            if not(o_e is None):
                lo_x.append(o_w)
        return(lo_x)

    def out_edges(self, o_v):
        """
        list of outer edges
        """
        lo_e = []
        lo_w = self.vertices()
        for o_w in lo_w:
            o_e = self.get_edge(o_v, o_w)
            if not(o_e is None):
                lo_e.append(o_e)
        return(lo_e)

    def add_all_edges(self):
        """
        take edgeless graph
        make complet graph
        """
        lo_v = list(self.keys())
        lo_w = lo_v
        for o_v in lo_v:
            for o_w in lo_w:
                if not(o_v == o_w):
                    o_e = Edge(o_v, o_w)
                    self.add_edge(o_e)
                    print("EDGE: ", o_e)
                else:
                    print("NOP: ", o_v, o_w)
        return(lo_v)

    def __init__(self, lo_v=[], lo_e=[]):
        """
        create a new graph.
        (lo_v) is a list of vertices;
        (lo_e) is a list of edges.
        input: list of vertexes and list of edge(s)
        """
        for o_v in lo_v:
            self.add_vertex(o_v)

        for o_e in lo_e:
            self.add_edge(o_e)


def main(script, *args):
    # vertex
    print("\n*** vertex ***")
    v = Vertex('v')
    print(v)
    w = Vertex('w')
    print(w)
    x = Vertex('x')
    print(x)
    y = Vertex('y')
    print(y)
    z = Vertex('z')
    print(z)

    # edges
    print("\n*** edge v w ***")
    e = Edge(v, w)
    print(e)

    # graph
    print("\n*** graph ***")
    g = Graph([v,w, x], [e])
    print(g)

    # 2.2.3 get edge in graph
    print("\n*** ged edge in graph v w ***")
    f = g.get_edge(v, w)
    print(f)
    print("\n*** ged edge in graph v x ***")
    f = g.get_edge(v, x)
    print(f)

    # 2.2.5 vertices
    print("\n*** list of vertices in a graph ***")
    print(g)
    vv = g.vertices()
    print(vv)

    # 2.2.6 edges
    print("\n*** list of edges in a graph ***")
    print(g)
    ee = g.edges()
    print(ee)

    # 2.2.7 outer vertices
    print("\n*** list of adjacent vertex ***")
    print(g)
    vv = g.out_vertices(v)
    print(vv)

    # 2.2.8 outer edges
    print("\n*** list of adjacent edges ***")
    print(g)
    ee = g.out_edges(v)

    # 2.2.4 remove edge
    print("\n*** remove edge in graph ***")
    print(g)
    g.remove_edge(e)
    print(g)

    # 2.2.9 add all edges
    print("\n*** add all edges ***")
    h = Graph([v, w, x, y, z],)
    h.add_all_edges()
    print(h)


if __name__ == '__main__':
    import sys
    main(*sys.argv)
